package com.multipart.alternative.mail.model;

import java.util.*;

/**
* <h1>Message</h1>
* <p>
* Multipart Alternative Model Class
* </p>
*
* @author  Thiago Santos <thiagosantos@thiagosantos.com.br>
* @version 1.0
* @since   2021-04-05 
*/
public class Message {
	private LinkedHashMap<String, String> headers;
	private LinkedHashMap<String, String> bodyParts;
	private LinkedHashMap<String, Object> serverConfig;
	private LinkedHashMap<String, String> attachments;
	
	private String serverUsername;
	private String serverPassword;
	private String subject;
	private String from;
	private String[] to;
	private String[] cc;
	private String[] bcc;
	private String[] replyTo;

	//Constructor
	public Message(LinkedHashMap<String, Object> serverConfig, LinkedHashMap<String, String> headers, LinkedHashMap<String, String> attachments, String serverUsername, String serverPassword, String subject, String from, String[] to, String[] replyTo, String[] cc, String[] bcc , LinkedHashMap<String, String> bodyParts) {
		this.setAttachments(attachments);
		this.setServerConfig(serverConfig);
		this.setServerUsername(serverUsername);
		this.setServerPassword(serverPassword);
		this.setSubject(subject);
		this.setHeaders(headers);
		this.setFrom(from);
		this.setTo(to);
		this.setReplyTo(replyTo);;
		this.setCC(cc);
		this.setBCC(bcc);
		this.setBodyParts(bodyParts);	
	}

	// Getter
	public LinkedHashMap<String, String> getAttachments() {
		return this.attachments;
	}

	public LinkedHashMap<String, Object> getServerConfig() {
		return this.serverConfig;
	}
	
	public String getServerUsername() {
		return this.serverUsername;
	}
	
	public String getServerPassword() {
		return this.serverPassword;
	}
	
	public LinkedHashMap<String, String> getHeaders() {
		return this.headers;
	}
	
	public LinkedHashMap<String, String> getBodyParts() {
		return this.bodyParts;
	}

	public String getSubject() {
		return this.subject;
	}
	
	public String getFrom() {
		return this.from;
	}
	
	public String[] getTo() {
		return this.to;
	}
	
	public String[] getReplyTo() {
		return this.replyTo;
	}
	
	public String[] getCC() {
		return this.cc;
	}

	public String[] getBCC() {
		return this.bcc;
	}	
	
	// Setter
	public void setAttachments(LinkedHashMap<String, String> newAttachments) {
		if(newAttachments == null)
			newAttachments = new LinkedHashMap<String, String>();  
		
		this.attachments = newAttachments;
	}
	
	public void setServerUsername(String newServerUsername) {
		this.serverUsername = newServerUsername;
	}
	
	public void setServerPassword(String newServerPassword) {
		this.serverPassword = newServerPassword;
	}

	public void setServerConfig(LinkedHashMap<String, Object> newServerConfig) {
		this.serverConfig = newServerConfig;
	}
	
	public void addServerConfig(String newKey, Object newValue) {
		if(this.serverConfig == null)
			this.serverConfig = new LinkedHashMap<String, Object>();

		if(!this.serverConfig.containsKey(newKey))
			this.serverConfig.put(newKey, newValue);
	}
	
	public void setBodyParts(LinkedHashMap<String, String> newBodyParts) {
		this.bodyParts = newBodyParts;
	}
	
	public void addBodyParts(String newKey, String newValue) {
		if(this.bodyParts == null)
			this.bodyParts = new LinkedHashMap<String, String>();
		
		if(!this.bodyParts.containsKey(newKey))
			this.bodyParts.put(newKey, newValue);
	}
	

	public void setHeaders(LinkedHashMap<String, String> newHeaders) {
		this.headers = newHeaders;
	}
	
	public void addHeader(String newKey, String newValue) {
		if(this.headers == null)
			this.headers = new LinkedHashMap<String, String>();
		
		if(!this.headers.containsKey(newKey))
			this.headers.put(newKey, newValue);
	}
	
	public void setSubject(String newSubject) {
		this.subject = newSubject;
	}
	
	public void setFrom(String newFrom) {
		this.from = newFrom;
	}
	
	public void setReplyTo(String[] newReplyTo) {
		if(newReplyTo == null)
			newReplyTo = new String[0];
		
		this.replyTo = newReplyTo;
	}
	
	public void setTo(String[] newTo) {
		if(newTo == null)
			newTo = new String[0];
		
		this.to = newTo;
	}

	public void setCC(String[] newCC) {
		if(newCC == null)
			newCC = new String[0];
		
		this.cc = newCC;
	}

	public void setBCC(String[] newBCC) {
		if(newBCC == null)
			newBCC = new String[0];
		
		this.bcc = newBCC;
	}
	
}