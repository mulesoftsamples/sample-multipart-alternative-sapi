package com.multipart.alternative.mail;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;

/**
* <h1>MessageTransport</h1>
* <p>
* This is a sample class used to send Multipart/Alternative emails through Mule 4
* </p>
*
* @author  Thiago Santos <thiagosantos@thiagosantos.com.br>
* @version 1.0
* @since   2021-04-05 
*/
public class MessageTransport {
	//Configurations
	final static String ALTERNATIVE = "alternative";
	final static String MIME_VERSION = "MIME-Version";
	final static String MIME_VERSION_VALUE = "1.0";
	final static String CONTENT_TYPE = "Content-Type"; 
	final static String CHARSET = "charset=iso-8859-1";
	final static String INLINE_DISPOSITION = "inline";
	final static String ATTACHMENT_DISPOSITION = "attachment";

	//Actions
	final static String SECTION_LINE = "====================================== ";
	final static String SERVER_CONFIGURATION = "Server Configuration";
	final static String SERVER_AUTH = "Server Authentication";
	final static String HEADERS = "Custom Headers";
	final static String ATTACHMENTS = "Attachments";
	final static String BODY_PARTS = "Body Parts";
	final static String ADDING = "Adding ";
	final static String PART = " part";
	final static String ANONYMOUS_AUTH_LOG = "Invoking Email Server Anonymously...";
	final static String BASIC_AUTH_LOG = "Performing Username and Password Authentication...";
	final static String MESSAGE_SENT = "The Multipart/Alternative email has been sent successfully!";

	
	//Exceptions
	final static String NO_SERVER_CONFIGURATION_EXCEPTION_MESSAGE = "Email Server Configuration Properties have not been provided.";
	final static String NO_SENDER_EXCEPTION_MESSAGE = "Message Sender Not Informed";
	final static String NO_RECIPIENT_EXCEPTION_MESSAGE = "Message Recipient Not Informed";

	
	private static String serverUsername;
	private static String serverPassword;
	
	   /**
	   * This static method is used to send Multipart/Alternative emails.
	   * @param message - Model Class with all email properties
	   * @return boolean - true, when message has been successfully sent or false in case of errors 
	   */
	public static boolean send(com.multipart.alternative.mail.model.Message message) throws NoSuchProviderException, MessagingException {
	
		try {
			MimeMultipart msgContent = new MimeMultipart(ALTERNATIVE);

			//Set Body Parts
		    System.out.println(SECTION_LINE);
		    System.out.println(BODY_PARTS);
		    System.out.println(SECTION_LINE);
			for(String key : message.getBodyParts().keySet()) {
				MimeBodyPart emailPart = new MimeBodyPart();
				emailPart.setContent( message.getBodyParts().get(key), key);
				emailPart.setHeader( MIME_VERSION, MIME_VERSION_VALUE);
				emailPart.setHeader( CONTENT_TYPE, key + "; " + CHARSET);
				emailPart.setDisposition(INLINE_DISPOSITION);
			    msgContent.addBodyPart( emailPart);
			    System.out.println(ADDING + key + PART);
			}
			
			//Set Attachments
		    System.out.println(SECTION_LINE);
		    System.out.println(ATTACHMENTS);
		    System.out.println(SECTION_LINE);
			for(String key : message.getAttachments().keySet()) {
				String[] fileParts = key.split(";");
				String filename = fileParts[0];
				String contentType = fileParts[1];
				String content = message.getAttachments().get(key);
				
				com.multipart.alternative.mail.model.Attachment file = new com.multipart.alternative.mail.model.Attachment(filename, contentType, content);
				
				MimeBodyPart attachmentsPart = new MimeBodyPart();
				attachmentsPart.setDataHandler(new DataHandler(new ByteArrayDataSource(file.getContent(), file.getContentType())));
				attachmentsPart.setDisposition(ATTACHMENT_DISPOSITION);
				attachmentsPart.setFileName(file.getFileName());
				msgContent.addBodyPart( attachmentsPart);
				
			    System.out.println(ADDING + key);
			}
			
			//Set Server Configuration
		    System.out.println(SECTION_LINE);
		    System.out.println(SERVER_CONFIGURATION);
		    System.out.println(SECTION_LINE);
			if(message.getServerConfig().isEmpty())
				throw new NoSuchProviderException(NO_SERVER_CONFIGURATION_EXCEPTION_MESSAGE);
				
			Properties props = System.getProperties();
			for(String key : message.getServerConfig().keySet()) {
				props.put(key, message.getServerConfig().get(key));
				
			    System.out.println(ADDING + key);
			}
			

			//Session 
			Session session;
			serverUsername = message.getServerUsername();
			serverPassword = message.getServerPassword();

		    System.out.println(SECTION_LINE);
		    System.out.println(SERVER_AUTH);
		    System.out.println(SECTION_LINE);

			if(serverUsername != null && serverPassword != null) {
			    System.out.println(BASIC_AUTH_LOG);
				
				//Username and password login
				session = Session.getDefaultInstance(props,
				          new javax.mail.Authenticator() {
				            	protected PasswordAuthentication getPasswordAuthentication() {
				            		return new PasswordAuthentication(serverUsername, serverPassword);
				            	}
							});
			}else {
			    System.out.println(ANONYMOUS_AUTH_LOG);
				
				//Anonymous permitted
				session = Session.getDefaultInstance(props);
			}
							
			MimeMessage email = new MimeMessage(session);
			
			//Set Custom Headers
		    System.out.println(SECTION_LINE);
		    System.out.println(HEADERS);
		    System.out.println(SECTION_LINE);
			for(String key : message.getHeaders().keySet()) {
				email.addHeader(key, message.getHeaders().get(key));
				
			    System.out.println(ADDING + key);
			}

			//Set Sender
			if(message.getFrom() == null)
				throw new MessagingException(NO_SENDER_EXCEPTION_MESSAGE);
			
			email.setFrom(message.getFrom());

			//Set Recipients
			if(message.getTo().length == 0)
				throw new MessagingException(NO_RECIPIENT_EXCEPTION_MESSAGE);
			
			for(String to : message.getTo()) {
				email.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));
			}
			
			//Set CC
			for(String cc : message.getCC()) {
				email.setRecipients(Message.RecipientType.CC,InternetAddress.parse(cc));
			}

			//Set BCC
			for(String bcc : message.getBCC()) {
				email.setRecipients(Message.RecipientType.BCC,InternetAddress.parse(bcc));
			}

			//Set Reply-To
			for(String replyTo : message.getReplyTo()) {
				email.setReplyTo(new javax.mail.Address[]{
						    new javax.mail.internet.InternetAddress(replyTo)
						});
			}
			
			email.setSubject(message.getSubject(), "UTF-8");		
			email.setContent(msgContent);
			
			
			//Send Email
			Thread.currentThread().setContextClassLoader(javax.mail.Message.class.getClassLoader());
			Transport.send(email);
			
		    System.out.println(MESSAGE_SENT);
			
			return true;
			
		}catch(Exception e) {
		    System.out.println(e.toString());
		    return false;
		}
	}

}
