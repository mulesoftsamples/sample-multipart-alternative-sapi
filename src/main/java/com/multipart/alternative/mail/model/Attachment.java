package com.multipart.alternative.mail.model;

import java.util.Base64;

/**
* <h1>Attachment</h1>
* <p>
* Attachment Model Class
* </p>
*
* @author  Thiago Santos <thiagosantos@thiagosantos.com.br>
* @version 1.0
* @since   2021-04-05 
*/
public class Attachment {
	private String fileName;
	private String contentType;
	private String base64Content;
	private byte[] content;
	
	//Constructor
	public Attachment(String fileName, String contentType, String base64Content) {
		this.setFileName(fileName);
		this.setContentType(contentType);
		this.setBase64Content(base64Content);
	}
	
	// Getter
	public String getFileName() {
		return this.fileName;
	}
	
	public String getContentType() {
		return this.contentType;
	}
	
	public String getBase64Content() {
		return this.base64Content;
	}
	
	public byte[] getContent() {
		return this.content;
	}
	
	// Setter
	public void setFileName(String newFileName) {
		this.fileName = newFileName;
	}
	
	public void setContentType(String newContentType) {
		this.contentType = newContentType;
	}
	
	public void setContent(byte[] newContent) {
		this.content = newContent;
	}
	
	public void setBase64Content(String newBase64Content) {
		this.base64Content = newBase64Content;
		this.setContent(Base64.getDecoder().decode(this.base64Content));

	}
}
